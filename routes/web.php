<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('pdf', function () {
    $pdf = PDF::loadView('vista');
    return $pdf->download('factura.pdf');
});





use MappingManagementSystem\Exports\BooksExport;//invocamos el export de BOOKS  
use Maatwebsite\Excel\Facades\Excel;

#// // // Route::get('exportarlibros', function () {
#// // //     return Excel::download(new BooksExport, 'books.xlsx');
#// // // });
 

 


// #########################--ADMINISTRACIÓN DE BOOKS--#####################
Route::resource('books' , 'bookController');   //asignamos las rutas automaticamente para que sea despachada por medio del controlador
Route::get('exportar', 'bookController@excel') ;// ruta para la exportacion dee libros en excell
Route::get('pdf', 'bookController@pdf') ;// ruta para la exportacion dee libros en excell
Route::get('books/{slug}/show', 'bookController@show') ->name('books.show');// ruta para la exportacion dee libros en excell
Route::get('books/{slug}/edit', 'bookController@edit') ->name('books.edit');// ruta para la exportacion dee libros en excell
Route::DELETE('books/{slug}/destroy', 'bookController@destroy') ->name('books.destroy');// ruta para la exportacion dee libros en excell
// Route::get('pdf', 'bookController@pdf') ;// ruta para la exportacion dee libros en excell

// #########################--ADMINISTRACIÓN DE USUARIOS--#####################


// // #########################--ADMINISTRACIÓN DE USUARIOS--#####################
// Route::get  ('/usuarios'                        , 'UserController@index');   
// Route::get  ('/usuarios/editar/{id}'            , 'UserController@edit');   
// // #########################--ADMINISTRACIÓN DE USUARIOS--#####################



// // #########################--ADMINISTRACIÓN DE PAISES--########################
Route::get  ('/paises'                          , 'CountriesController@index');   
Route::get  ('/paises/new/'                     , 'CountriesController@new');
// Route::get ('/paises/create'                   , 'PaisesController@create');
Route::post ('/paises/create'                   , 'CountriesController@create');
// // #########################--ADMINISTRACIÓN DE PAISES--#########################



// #########################--RUTAS DEL SISTEMA--###############################
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// #########################--RUTAS DEL SISTEMA--###############################