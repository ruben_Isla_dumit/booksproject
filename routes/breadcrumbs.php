<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home / Books
Breadcrumbs::for('books.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Books', route('books.index'));
});

// Home / Books / Create
Breadcrumbs::for('books.create', function ($trail) {
    $trail->parent('books.index');
    $trail->push('Create', route('books.create'));
});
 

// Home / Books / Edit / $slug
Breadcrumbs::for('books.edit', function ($trail,$books) {
    $trail->parent('books.index');
    $trail->push('Edit: '.$books  , route('books.edit', $books  ));
});

// Home / Books / Show / $slug
Breadcrumbs::for('books.show', function ($trail,$books) {
    $trail->parent('books.index');
    $trail->push('Show: '.$books  , route('books.show', $books  ));
});
 
 