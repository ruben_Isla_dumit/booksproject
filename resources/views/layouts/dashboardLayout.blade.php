<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- <title> Proyecto - @yield('title')  </title> -->
        <title>{{ config('app.name', 'Laravel') }}</title>   

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- ---------------------------------estilos de boostrap--------------------------------------------- -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>


        <link href="{{ asset('boostrap/css/bootstrap.css') }}" rel="stylesheet">

        <!-- ---------------------------------estilos de boostrap--------------------------------------------- -->


        <link href="{{ asset('img/dumit.ico') }}" rel="shortcut icon" >


        <!-- --------------------------------- Estilos de la plantilla-------------------------------------------- -->
            <!-- <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet"> -->
            <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
            <link href="{{ asset('css/fontastic.css') }}" rel="stylesheet">
            <link href="{{ asset('css/grasp_mobile_progress_circle-1.0.0.min.css') }}" rel="stylesheet">
            <!-- <link href="{{ asset('css/style.blue.css') }}" rel="stylesheet"> -->
            <link href="{{ asset('css/style.default.css') }}" rel="stylesheet">
            <!-- <link href="{{ asset('css/style.green.css') }}" rel="stylesheet"> -->
            <!-- <link href="{{ asset('css/style.pink.css') }}" rel="stylesheet"> -->
            <!-- <link href="{{ asset('css/style.red.css') }}" rel="stylesheet"> -->
            <!-- <link href="{{ asset('css/style.sea.css') }}" rel="stylesheet"> -->
            <!-- <link href="{{ asset('css/style.violet.css') }}" rel="stylesheet"> -->
        <!-- --------------------------------- Estilos de la plantilla-------------------------------------------- -->

        <meta name="robots" content="all,follow">
        <meta name="csrf-token" content="{{ csrf_token() }}">
            
        <!-- repositorio de iconos -->
        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    </head>
    <body>

        
        <!-- Side Navbar -->
        <nav class="side-navbar">
            <div class="side-navbar-wrapper">
                @include('components/sideBarHeader')
                @include('components/sideBarLeft')
                @include('components/sideBarSecMenu')
            </div>
        </nav>
        
        <div class="page">
            
            <!-- navbar-->     
                @include('components/navBar')
            <!-- navbar-->

            <!-- Counts Section -->
                @yield('content')
            <!-- Counts Section -->
             
        </div>

        <!-- footer Section -->
        @include('components/footer')
        <!-- footer Section -->
 


         <!-- JavaScript files-->
         <script src="{{ asset('boostrap/js/bootstrap.min.js') }}"></script>

        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"></script>
        
        <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"></script>
        <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
        <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('js/charts-home.js') }}"></script>
     
        <!-- Main File-->
        <script src="{{ asset('js/front.js') }}"></script>

        <!-- personalizado -->
        <script src="{{ asset('js/personalizado.js') }}"></script>
        

    </body>
</html>
