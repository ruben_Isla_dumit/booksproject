<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- <title> Proyecto - @yield('title')  </title> -->
        <title>{{ config('app.name', 'Laravel') }}</title>   

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--------------------------------------  begin FAV-ICONOS------------------------------------------------>
        <link href="{{ asset('img/dumit.ico') }}" rel="shortcut icon" 
        <!--------------------------------------  begin FAV-ICONOS--------------------------------------------------->


        <!-- ---------------------------------estilos de boostrap---------------------------------------------------->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
        <!-- ---------------------------------estilos de boostrap---------------------------------------------------->

        <!--------------------------------------  begin Web fon------------------------------------------------------>
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        {{--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  --}}
        <!-----------------------------------------end Web font------------------------------------------------------>
       
        <!--------------------------------------  begin Base Styles-------------------------------------------------->
            <!--------------------------------------  begin Page Vendors--------------------------------------------->
                <link href="{{ asset('vendor/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" >
            <!--------------------------------------  END Page Vendors----------------------------------------------->

            <link href="{{ asset('vendor/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" >
            <link href="{{ asset('demo/demo9/base/style.bundle.css') }}" rel="stylesheet" type="text/css" >
        <!-----------------------------------------end Base Styles-------------------------------------------------->

        
        <!--------------------------------------  begin ICONOS------------------------------------------------------>
        <link href="{{ asset('css/all.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        {{-- <link rel="stylesheet" href="{{ asset('fonts/metronic/Metronic_fda1334c35d0f5fe2afb3afebbb6774a.woff') }}" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> --}}
        <!-----------------------------------------end ICONOS---------------------------------------------------- -->


        <!--------------------------------------  begin Web TOKENS------------------------------------------------->
        <meta name="robots" content="all,follow">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-----------------------------------------end Web TOKENS---------------------------------------------- -->


    </head>
    <body class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default" >


        <!-- Page loader-->     
            @include('components/loader')
        <!-- Page loader-->

        
        
        <!-- begin Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

            <!-- BEGIN: Header -->
            <header id="m_header" class="m-grid__item    m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop  m-header__wrapper">
            
                        <!-- brand-->     
                            @include('components/brand')
                        <!-- brand-->

                        <!-- navbar-->     
                            @include('components/navBar')
                        <!-- navbar-->
                        
                        <!-- Brand 2-->     
                            @include('components/Brand2')
                        <!-- Brand 2-->

                        <!-- TopBar-->     
                            @include('components/TopBar')
                        <!-- TopBar-->

                    </div>
                </div>
            </header>

            <!-- end  header -->

            <!-- sideBarLeft-->     
                @include('components/sideBarLeft')
            <!-- sideBarLeft-->            

            <!-- begin Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
					<div class="m-grid__item m-grid__item--fluid m-wrapper">
                        
                        <!-- breadCrumbs-->     
                            @include('components/breadCrumbs')
                        <!-- breadCrumbs--> 
                        
                        <div class="m-content">
                            <div class="row"> 
                                <!-- Counts Section -->
                                    @yield('content')
                                <!-- Counts Section -->
                            </div>  
                        </div> 

                    </div>            
                </div>            
            </div>            
            <!--END body-->

            <div class="card p-2 " style="background-color: #2c2e3e; border-radius:0px;">
                <!-- footer-->     
                @include('components/footer')
            <!-- footer-->
            </div>

        </div>
        <!-- end  Page -->

        {{--  <!-- sideBarRight-->     
            @include('components/sideBarRight')
        <!-- sideBarRight-->  --}}

        <!-- begin Scroll To Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>
		<!-- end Scroll To Top -->		

        <!-- widgetRight-->     
            @include('components/widgetRight')
        <!-- widgetRight-->

        <!--begin::Base Scripts -->
        <script src="{{ asset('vendor/base/vendors.bundle.js') }}"></script>
        <script src="{{ asset('demo/demo9/base/scripts.bundle.js') }}"></script>
        <!--end::Base Scripts -->
           
        <!--begin::Page Vendors -->  
        <script src="{{ asset('vendor/base/fullcalendar.bundle.js') }}"></script>
        <!--end::Page Vendors -->
          
        <!--begin::Page Snippets -->
        <script src="{{ asset('app/js/dashboard.js') }}"></script>
		<!--end::Page Snippets -->   

         <!-- JavaScript files-->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"></script>
        <s cript src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
         


        <!-- begin::Page Loader -->
		<script>
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');         
            });
		</script>
		<!-- end::Page Loader -->
     
        <!-- Main File-->
        <script src="{{ asset('js/front.js') }}"></script>
        
        <!-- personalizado -->
        <script src="{{ asset('js/personalizado.js') }}"></script>
        
    </body>
</html>
