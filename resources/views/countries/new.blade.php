 
 @extends ('layout') <!-- Extendemos la plantiya de layout principal -->
     
@section('content') 


<br>
<br>
<br>

    <div class="container ">
        <div class="row">
            
            <form method="POST" action="{{ url('paises/create') }}">

                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="pais">Pais: </label>
                    <input type="text" name="pais" class="form-control" id="pais" placeholder="ingrese el pais">
                </div>
                <div class="form-group">
                    <label for="ciudad">Ciudad:</label>
                    <input type="text" name="ciudad"  class="form-control" id="ciudad" placeholder="ingrese la ciudad ">
                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
                
        </div>
    </div>

@endsection