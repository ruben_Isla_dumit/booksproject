<!-- BEGIN: Brand 1-->
	<div class="m-stack__item m-brand m-brand--mobile">
		<div class="m-stack m-stack--ver m-stack--general">
			<div class="m-stack__item m-stack__item--middle m-brand__logo">
				<a href="index.html" class="m-brand__logo-wrapper img-fluid">
					<img class=" img-fluid" alt="" src="img/dumit_logo.jpg"/>
				</a>
			</div>
			<div class="m-stack__item m-stack__item--middle m-brand__tools">
				
				<!-- BEGIN: Responsive Aside Left Menu Toggler -->
				<a href="javascript:;" id="m_aside_left_toggle_mobile" class="m-brand__icon m-brand__toggler m-brand__toggler--left">
					<span></span>
				</a>
				<!-- END -->

				{{-- <!-- BEGIN: Responsive Header Menu Toggler -->
				<a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler">
					<span></span>
				</a>
				<!-- END --> --}}

				<!-- BEGIN: Topbar Toggler -->
				<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon">
					<i class="flaticon-more"></i>
				</a>
				<!-- END: Topbar Toggler -->
				
			</div>
		</div>
	</div>
<!-- END: Brand 1-->