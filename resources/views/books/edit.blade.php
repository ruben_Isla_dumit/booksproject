 
@extends ('layouts/dashboardLayout2')<!-- Extendemos la plantiya de layout principal -->
     
@section('content') 


<br>
<br>
<br>

    <div class="container  ">       
        <div class="row ">
            
            <form class="col-5 card p-5 mx-auto" action="{{route('books.update', $book->id)}}" method="POST" >
                
                <p> <h3 class="text-center">Edit a This Book</h3></p>
                @method('put')<!-- se le pasa el meotodo PUT para el update -->
                @csrf <!-- se pasa un token por debajo para evitar los ataques CSRF -->
                              
                <div class="form-group ">
                    <label for="pais">Tittle: </label>
                    <input type="text" name="tittle" class="form-control" id="tittle" placeholder="ingrese el titulo" value="{{$book->tittle}}">
                </div>

                <div class="form-group">
                    <label for="author">Author:</label>
                    <input type="text" name="author"  class="form-control" id="author" placeholder="ingrese el author " value="{{$book->author}}">
                </div>
                 
                 
                <div class="btn-group   btn-group-lg" style=" ">
                    <button class="btn btn-primary btn-lg"  type="submit"  >Submit</button>
                    <a      class="btn btn-secondary btn-lg" href="{{ route('books.index') }}"><i class="fas fa-undo"></i> Back</a>
                </div>
                 
                 
            </form>
                
        </div>
    </div>

@endsection