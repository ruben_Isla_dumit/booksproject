 
@extends ('layouts/dashboardLayout2')<!-- Extendemos la plantiya de layout principal -->
     
@section('content') 


<br>
<br>
<br>

    <div class="container  ">       
        <div class="row ">
            
            <div class="col-5 card p-5 mx-auto"   >
                
                <p> <h3 class="text-center">Showing this Book</h3></p>
                @method('put')<!-- se le pasa el meotodo PUT para el update -->
                @csrf <!-- se pasa un token por debajo para evitar los ataques CSRF -->
                              
                <div class="form-group ">
                    <label for="pais"><strong>Tittle:</strong> </label>
                    <input type="text" name="tittle" class="form-control" id="tittle"  value="{{$book->tittle}}"  readonly=”readonly” >
                </div>

                <div class="form-group">
                    <label for="author"><strong>Author:</strong></label>
                    <input type="text" name="author"  class="form-control" id="author"  value="{{$book->author}}"  readonly=”readonly” >
                </div>
                 
                 
                <div class="btn-group btn-group-sm">
                    <a class="btn btn-info" href="{{ route('books.index') }}"><i class="fas fa-undo"></i> Return Back</a>
                </div>
                 
                 
            </div>
                
        </div>
    </div>

@endsection