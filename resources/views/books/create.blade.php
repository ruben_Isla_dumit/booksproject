 
@extends ('layouts/dashboardLayout2')<!-- Extendemos la plantiya de layout principal -->
     
@section('content') 


<br>
<br>
<br>

    <div class="container  ">       
        <div class="row ">
            
            <form class="col-5 card p-5 mx-auto" method="POST" action="{{route('books.store')}}">
                
                <p> <h3 class="text-center">Create a New Book</h3></p>

                @csrf <!-- se pasa un token por debajo para evitar los ataques CSRF -->
                              
                <div class="form-group ">
                    <label for="pais">Tittle: </label>
                    <input type="text" name="tittle" class="form-control" id="tittle" placeholder="ingrese el titulo">
                </div>

                <div class="form-group">
                    <label for="author">Author:</label>
                    <input type="text" name="author"  class="form-control" id="author" placeholder="ingrese el author ">
                </div>
                 
                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-primary ">Submit</button>
                    <a class="btn btn-secondary  " href="{{ route('books.index') }}"><i class="fas fa-undo"></i> Back</a>
                </div>
                
               

            </form>
                
        </div>
    </div>

@endsection