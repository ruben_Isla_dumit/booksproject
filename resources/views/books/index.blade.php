@extends ('layouts/dashboardLayout2')
<!-- Extendemos la plantiya de layout principal -->

@section('content')
<br><br>  
        <div class="container-fluid ">
                @if(Session::has('success')) 
                        <div class="col-6 alert alert-success text-center mx-auto alert-dismissible" style="border-radius: 0px;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Success!</strong> {{session::get('success')}}
                        </div>
                @elseif (Session::has('alert'))
                        <div class="col-6 alert alert-danger text-center mx-auto alert-dismissible" style="border-radius: 0px;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Alert!</strong> {{session::get('alert')}}
                        </div>
                @endif
                <div class="container-fluid"  >
                        
                        <div class="row  btn-group m-4  float-right push-right"  >
                                <a   class="  btn btn-primary float-right push-right " type="button" href="{{route('books.create')}}">Create New Book <i class="fas fa-plus-circle m-1"></i></a><br>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class=" row  m-4 p-2 b-1  ">
                                
                                    
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs btn-group col-12 " style="color: #734cea;border-bottom: 1px solid #734cea;">
                                                <li class="nav-item">
                                                        <a class="   active btn btn-primary  btn-sm" data-toggle="tab" href="#listView">List View    <i class="fas fa-list-ul m-1"></i></a>
                                                </li>
                                                <li class="nav-item">
                                                        <a class="  btn btn-primary  btn-sm" data-toggle="tab" href="#kanbanView">Kanban View    <i class="fas fa-id-card-alt m-1"></i></a>
                                                </li>
                                                {{-- <li class="nav-item">
                                                        <a class="  btn btn-primary  btn-sm" data-toggle="tab" href="#graphsView">Graphs View    <i class="fas fa-chart-pie m-1"></i></a>
                                                </li> --}}
                                                <br>
                                                <br>
                                        </ul>
                                        
                                      
                                        <!-- Tab panes -->
                                        <div class="tab-content col-12  bg-secondary  rounded "  style=" border: 1px solid #734cea; " >
                                                <div id="listView" class=" col-12 tab-pane active"><br>
                                          
                                                        <div class="col-12  ">
                                                                <P><strong><h2 class="ml-5">Titulo de Tabla</h2></strong></p>
                                                                <table class="table table-striped table-bordered table-hover table-sm text-center">
                                                                        <thead class="bg-dark text-light">
                                                                                <tr>
                                                                                        <th>id</th>
                                                                                        <th>Tittle</th>
                                                                                        <th>Author</th>
                                                                                        <th>Slug</th>
                                                                                        <th>Options</th>
                                                                                </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                                @foreach($books as $book )
                                                                                        <tr>
                                                                                                <td class="align-middle">{{$book->id}}</td>
                                                                                                <td class="align-middle">{{$book->tittle}}</td>
                                                                                                <td class="align-middle">{{$book->author}}</td>
                                                                                                <td class="align-middle">{{$book->slug}}</td>
                                                                                                <td class="align-middle">
                                                                                                        <div class="dropdown">
                                                                                                                <button type="button" class="btn btn-success dropdown-toggle  " data-toggle="dropdown">
                                                                                                                        Options   
                                                                                                                </button>
                                                                                                                <div class="dropdown-menu">
                                                                                                                        {{-- <a class="dropdown-item" href="{{route('books.edit' ,$book->slug)}}">Edit</a> --}}
                                                                                                                        <a class="dropdown-item" href="books/{{ $book->slug  }}/show">show</a>
                                                                                                                        <a class="dropdown-item" href="books/{{ $book->slug  }}/edit">Edit</a>
                                                                                                                        {{-- <form action="{{route('books.destroy',$book->slug)}}" method="post"> --}}
                                                                                                                        <form action="{{route('books.destroy',$book->slug)}}" method="post">
                                                                                                                                @csrf
                                                                                                                                @method('DELETE')
                                                                                                                                <button class="dropdown-item  " type="submit">Delete</button>
                                                                                                                        </form>
                                                                                                                        <a class="dropdown-item" href="#">PDF Report</a>
                                                                                                                </div>
                                                                                                                
                                                                                                        </div>
                                                                                                </td>
                                                                                        </tr>
                                                                                @endforeach
                                                                        </tbody>
                                                                        
                                                                </table>
                                                                {{ $books->links() }}
                                                        </div>
                                        
                                                </div>
                                                <div id="kanbanView" class="container tab-pane fade"><br>
                                                        
                                                        @foreach($books as $book )
                                                                 
                                                        <div class="container p-1 m-1 border  col-3">
                                                                <div class="card card-group " >
                                                                  
                                                                <div class="card  " style="width:200px; " >
                                                                        <div class="card-body">
                                                                                <h4 class="card-title mx-auto" style="text-align: center;"><strong>{{$book->tittle}}</strong></h4>
                                                                                <p>{{ $book->author }}</p>

                                                                                <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer ...</p>
                                                                               
                                                                        </div>
                                                                        <img class="card-img-bottom img-fluid mx-auto d-block m-1 " src="{{ asset('img/books.jpg') }}" alt="Card image" style="max-width:200px; width:auto; max-height:200px;  height: auto;">
                                                                        <button type="button" class="btn btn-primary  btn-sm m-1" data-toggle="modal" data-target="#myModal">
                                                                                Details
                                                                        </button>
                                                                </div>

                                                                <!-- The Modal -->
                                                                <div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true" id="myModal">
                                                                        <div class="modal-dialog modal-dialog-centered " role="document">
                                                                                <div class=" card modal-content  " style=" 
                                                                                        box-shadow: 0px 0px 0px 7px rgba(46,64,212,.3);">
                                                                                        
                                                                                        <div class="modal-header" style="  ">
                                                                                                <img class="card-img-bottom img-fluid mx-auto d-block m-2 " src="{{ asset('img/books.jpg') }}" alt="Card image" style="max-width:200px; width:auto; max-height:200px;  height: auto;">
                                                                                        </div>

                                                                                        <div class="modal-body text-white btn  " style=" background: rgba(46,64,212,.6);border: 1px solid #734cea;">
                                                                                                <h5 class="modal-title" id="my-modal-title"><strong>Jane Doe</strong></h5><br>
                                                                                                <p class="card-text">Some example text some example text. Jane Doe is an architect and engineer ...</p>
                                                                                        </div>

                                                                                        <div class="modal-footer" style="  ">
                                                                                                <div class="btn-group   btn-group-sm" style=" ">
                                                                                                        <a class="btn btn-primary btn-sm active show" href="books/{{ $book->slug  }}/edit">Edit</a>
                                                                                                        <form action="{{route('books.destroy',$book->slug)}}" method="post">
                                                                                                                @csrf
                                                                                                                @method('DELETE')
                                                                                                                <button class="btn btn-secondary btn-sm" type="submit">Delete</button>
                                                                                                        </form>
                                                                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                                                                               </div>
                                                                                        </div>

                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <!-- END Modal -->
                                                        </div>
                                                        @endforeach


                                                </div>
                                                <div id="graphsView" class="container tab-pane fade"><br>
                                                        
                                                        <div class="container p-1 m-1 border border-primary col-3">
                                                                vista de graficos
                                                        </div>

                                                </div>
                                          
                                        </div>
                                        
                        </div>                      


                        
                </div>
        </div>


<br>
<br>


<span class="m-list-settings__item-control">
        <span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand checked">
                <label>
                        <input type="checkbox" name="">
                        <span></span>
                </label>
        </span>
</span>

@endsection
