<?php

namespace MappingManagementSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountriesController extends Controller {

    public function index() {

        $countries = DB::table('countries')->get();
        // dd($countries);
        // $tittle= "Lista de Paises";
       
        return view('countries.index', [ 'paises' => $countries ] );
    }

     
}
