<?php

namespace MappingManagementSystem\Http\Controllers;
// use MappingManagementSystem\Http\Controllers\Controller;
use MappingManagementSystem\Book;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

// uso de la paginacion
use Illuminate\Pagination\Paginator;
// uso de la paginacion

// uso del exportador de excel
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\Exportable;
use MappingManagementSystem\Exports\BooksExport;
// uso del exportador de excel

// uso de los slugs
Use Cviebrock\EloquentSluggable\Sluggable;
// uso de los slugs

use MappingManagementSystem\Http\Requests\books\BooksStoreRequest;
use MappingManagementSystem\Http\Requests\books\BooksUpdateRequest;

 
class BookController extends Controller {

    use Exportable;


    public function excel( ) {
        

        
        return (new BooksExport)->download('invoices.xlsx');
        // return redirect()->action('bookController@create');
    }
 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdf() {

        $books = Book::all();

        $pdf = \PDF::loadView('books/vista', ['books' => $books]);
        return $pdf->stream('factura.pdf');

    }

    public function index() {

        $books = Book::orderBy('id','desc')->paginate(10);// se trae la información del base de datos, se ordena por id de forma ascendente y se pasa la paginación
        return view('books.index', [ 'books' => $books ] );// se retorna la vista y se le manda el array de la información buscada

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        
        return view('books.create');// se retorna la vista con el formulario para crear el nuevo articulo

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BooksStoreRequest $request){

        $validatedData = $request->validate([ //validamos que requiera el contenido para enviar el formulario
            'tittle' => 'required| max:10',
            'author' => 'required| max:10',
        ]);

        // se registra los  datos que se reciven por el request

        Book::create($validatedData) ;

        return redirect('books/')->with('Success','The new book was  added successfully!'); 
        
    }


    /**
     * Display the specified resource.
     *
     * @param  \MappingManagementSystem\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(  $slug)
    {
        
        $book = book::where('slug','=',$slug)->FirstOrFail();
       
        // return view('books.edit', [ 'book' => $book ] );
        return view('books.show',   [ 'book' => $book ]);
    
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \MappingManagementSystem\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($slug){
        
        $book = book::where('slug','=',$slug)->FirstOrFail();
        
        // return view('books.edit', [ 'book' => $book ] );
        return view('books.edit',  compact('book'));
    }


    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \MappingManagementSystem\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(BooksUpdateRequest $request, Book $book)
    {
        $request->validate([
            'tittle' => 'required',
            'author' => 'required',
        ]);
  
        if ($book->update($request->all())){
            return redirect()->route('books.index')->with('success','Book updated successfully');
        }else{
            return redirect()->route('books.index')->with('alert',' Problems updating book');
        }
  
       
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \MappingManagementSystem\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug){
        
        $book = book::where('slug','=',$slug)->FirstOrFail();
        if ($book->delete()){
            return redirect()->route('books.index')->with('success','Book deleted successfully');
        }else{
            return redirect()->route('books.index')->with('alert',' Problems deleting  book');
        }
  
    }
}
