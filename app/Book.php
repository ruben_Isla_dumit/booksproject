<?php

namespace MappingManagementSystem;

use Illuminate\Database\Eloquent\Model;
Use Cviebrock\EloquentSluggable\Sluggable;
 



class Book extends Model {

    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {

        // el campo se va a llenar cuando se ingrese informacion en el camppo "tittle" (la fuente puede ser cualquier campo)
        return [ 'slug' => [ 'source' => 'tittle','save_to' => 'slug'] ];
        
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','tittle', 'author','slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];
}

 