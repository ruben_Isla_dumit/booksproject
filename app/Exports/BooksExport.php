<?php

namespace MappingManagementSystem\Exports;

use MappingManagementSystem\Book;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class BooksExport implements FromCollection
{

    use exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Book::all();
        
    } 
}
